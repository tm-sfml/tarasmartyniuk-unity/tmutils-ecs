﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using Unity.Entities;

namespace SmokGnu.EcsUtils
{
    public static class SerializationUtils
    {
        // public static string Serialize(int2 v) => $"{v.x},{v.y}";
        // public static int2 DeserializeInt2(string str)
        // {
        //     var tokens = str.Split(',');
        //     int x = int.Parse(tokens[0]);
        //     int y = int.Parse(tokens[1]);
        //     return new int2(x, y);
        // }
        //
        // public static string Serialize(int4 v) => $"{v.x},{v.y},{v.z},{v.w}";
        // public static int4 DeserializeInt4(string str)
        // {
        //     var tokens = str.Split(',');
        //     int4 result = new();
        //     for (int i = 0; i < 4; i++)
        //         result[i] = int.Parse(tokens[i]);
        //
        //     return result;
        // }
        //
        // public static string Serialize(int3 v) => $"{v.x},{v.y},{v.z}";
        // public static int3 DeserializeInt3(string str)
        // {
        //     var tokens = str.Split(',');
        //     int3 result = new();
        //     for (int i = 0; i < 3; i++)
        //         result[i] = int.Parse(tokens[i]);
        //
        //     return result;
        // }
        //
        // public static string Serialize(float3 v) => $"{v.x},{v.y},{v.z}";
        // public static float3 DeserializeFloat3(string str)
        // {
        //     var tokens = str.Split(',');
        //     float x = float.Parse(tokens[0]);
        //     float y = float.Parse(tokens[1]);
        //     float z = float.Parse(tokens[2]);
        //     return new float3(x, y, z);
        // }
    
        // public static string Serialize(float2 v) => $"{v.x},{v.y}";
        // public static float2 DeserializeFloat2(string str)
        // {
        //     var tokens = str.Split(',');
        //     float x = float.Parse(tokens[0]);
        //     float y = float.Parse(tokens[1]);
        //     return new float2(x, y);
        // }


        public static DynamicBuffer<T> DeserializeBuffer<T>(JToken save, Entity entity, string key, JsonSerializer serializer)
            where T : unmanaged, IBufferElementData => DeserializeBuffer<T, T>(save, entity, key, serializer);
    
        public static DynamicBuffer<TSerialized> DeserializeBuffer<T, TSerialized>(JToken save, Entity entity, string key, JsonSerializer serializer)
            where T : unmanaged, IBufferElementData
            where TSerialized : unmanaged
        {
            var eManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            var shipQueueSave = save[key].ToObject<TSerialized[]>(serializer);
            var buffer = eManager.GetBuffer<T>(entity).Reinterpret<TSerialized>();
            buffer.CopyFrom(shipQueueSave);
            return buffer;
        }

        public static DynamicBuffer<T> DeserializeBufferFromString<T>(JToken save, Entity entity, string key, JsonSerializerSettings serializerSettings) 
            where T : unmanaged, IBufferElementData =>
            DeserializeBufferFromString<T, T>(save, entity, key, serializerSettings);

        public static DynamicBuffer<TSerialized> DeserializeBufferFromString<T, TSerialized>(JToken save, Entity entity, string key, JsonSerializerSettings serializerSettings)
            where T : unmanaged, IBufferElementData
            where TSerialized : unmanaged
        {
            var eManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            var saveArray = JsonConvert.DeserializeObject<TSerialized[]>(save.Value<string>(key), serializerSettings);
            var buffer = eManager.GetBuffer<T>(entity).Reinterpret<TSerialized>();
            buffer.CopyFrom(saveArray);
            return buffer;
        }

        public static T DeserializeComponent<T>(JToken save, Entity entity, string key, JsonSerializer serializer)
            where T : unmanaged, IComponentData
        {
            var eManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            var cmp = save[key].ToObject<T>(serializer);
            eManager.SetComponentData(entity, cmp);
            return cmp;
        }
    
        public static T SerializeComponent<T>(JToken save, Entity entity, string key, JsonSerializer serializer)
            where T : unmanaged, IComponentData
        {
            var eManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            var cmp = eManager.GetComponentData<T>(entity);
            save[key] = JObject.FromObject(cmp, serializer);
            return cmp;
        }
    
        public static void SerializeBuffer<T>(JToken save, Entity entity, string key, JsonSerializer serializer)
            where T : unmanaged, IBufferElementData => SerializeBuffer<T, T>(save, entity, key, serializer);
    
        public static void SerializeBuffer<T, TSerialized>(JToken save, Entity entity, string key, JsonSerializer serializer)
            where T : unmanaged, IBufferElementData
            where TSerialized : unmanaged
        {
            var eManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            save[key] = JArray.FromObject(eManager.GetBuffer<T>(entity).Reinterpret<TSerialized>().ToArray(), serializer);
        }
    
        public static void SerializeBufferToString<T, TSerialized>(JToken save, Entity entity, string key, JsonSerializerSettings settings)
            where T : unmanaged, IBufferElementData
            where TSerialized : unmanaged
        {
            throw new NotImplementedException();
            var eManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            save["ColumnToQueueNumber"] =
                JsonConvert.SerializeObject(eManager.GetBuffer<T>(entity).Reinterpret<TSerialized>().AsNativeArray().ToArray(), Formatting.None, settings);
        }
    
    }
}
