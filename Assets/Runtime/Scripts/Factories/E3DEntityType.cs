﻿namespace SmokGnu.EcsUtils.Factories
{
    public enum E3DEntityType
    {
        Invalid,
        Root,
        Parent,
        Child,
        Count
    }
}
