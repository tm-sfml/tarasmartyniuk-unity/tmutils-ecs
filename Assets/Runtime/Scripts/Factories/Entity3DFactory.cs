﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using SmokGnu.EcsUtils.Debug;
using TMUtils.Utils.Collections;
using TMUtils.Utils.Math;

namespace SmokGnu.EcsUtils.Factories
{
    public class Entity3DFactory
    {
        public EntityDefinition DefinitionScale3D { get; }
        public EntityDefinition Definition { get; }
        private EntityManager EntityManager => World.DefaultGameObjectInjectionWorld.EntityManager;

        public Entity3DFactory()
        {
            Definition = new EntityDefinition(new ComponentType[]
            {
                typeof(LocalTransform), typeof(LocalToWorld),
            });

            DefinitionScale3D =
                new EntityDefinition(Definition.ComponentTypes.Concat(typeof(PostTransformMatrix)));
        }

        public Entity Create3DEntity(string name = "dummy",
            float3? scale3D = null,
            Entity? parent = null)
        {
            var entity = EntityManager.CreateEntity(name, DefinitionScale3D.Archetype);

            Init(entity, scale3D);
            if (parent != null)
            {
                EntityManager.AddComponentData(entity, new Parent { Value = parent.GetValueOrDefault(Entity.Null) });
            }

            return entity;
        }

        public void Init(Entity entity3D, float3? scale3D = null)
        {
            EntityManager.SetComponentData(entity3D, LocalTransform.Identity);

            scale3D ??= Float3V.One;
            var scaleMatrix = float4x4.Scale(scale3D.Value);
            EntityManager.SetComponentData(entity3D, new PostTransformMatrix { Value = scaleMatrix });

            // cmp_err?
            // EntityTransformUtils.SetRootPositionUpdateLTW(entity, float3.zero);
        }
    }
}