﻿using Unity.Entities;

namespace SmokGnu.EcsUtils.Factories
{
    public static class EntityCreationUtils
    {
        public static Entity CreateEntityWithComponent<T>(this EntityManager eManager, T component, EntityArchetype archetype)
            where T : unmanaged, IComponentData
        {
            var entity = eManager.CreateEntity(archetype);
            eManager.SetComponentData(entity, component);
            return entity;
        }

        public static Entity CreateEntityWithComponent<T>(this EntityManager eManager, T component)
            where T : unmanaged, IComponentData
        {
            var entity = eManager.CreateEntity();
            eManager.AddComponentData(entity, component);
            return entity;
        }
    }
}
