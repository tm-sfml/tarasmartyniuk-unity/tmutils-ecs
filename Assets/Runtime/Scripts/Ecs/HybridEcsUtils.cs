﻿using Unity.Entities;
using UnityEngine;

namespace SmokGnu.EcsUtils.Ecs
{
    public static class HybridEcsUtils
    {
        public static bool HasLinkedGameObject(this EntityManager eManager, Entity entity) =>
            eManager.HasComponent<Transform>(entity) ;

        public static GameObject GetLinkedGameObject(this EntityManager entityManager, Entity entity)
        {
            return entityManager.GetComponentObject<Transform>(entity).gameObject;
        }
    
        public static void Destroy(Entity entity, GameObject gameObject)
        {
            Object.Destroy(gameObject);
            var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            entityManager.DestroyEntity(entity);
        }
    }
}