using System;
using TMUtilsEcs.Ecs;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public class TransformSetter
{
    private readonly EntityManager _entityManager;
    private readonly SystemApiServiceSystem _systemApi;

    public TransformSetter(
        EntityManager entityManager,
        SystemApiServiceSystem systemApi)
    {
        _entityManager = entityManager;
        _systemApi = systemApi;
    }

    public void SetLocalPosition(Entity entity, float3 localPosition)
    {
        throw new NotImplementedException();
        var parent = _entityManager.GetComponentData<Parent>(entity).Value;

        ref var parentLocalToWorld = ref _systemApi.GetComponentRef<LocalToWorld>(parent);
        var worldPosition = localPosition * parentLocalToWorld.Position;
        
        ref var localTransform = ref _systemApi.GetComponentRef<LocalTransform>(entity);
        localTransform.Position = localPosition;
        
        ref var LocalToWorld = ref _systemApi.GetComponentRef<LocalToWorld>(entity);
        // LocalToWorld.Position = worldPosition;
    }
    
    public void SetWorldPosition(Entity entity, float3 worldPosition)
    {
        var localPosition = worldPosition;
        
        if (_entityManager.HasComponent<Parent>(entity))
        {
            var parent = _entityManager.GetComponentData<Parent>(entity).Value;
            if (parent != Entity.Null)
            {
                var parentLocalToWorld = _entityManager.GetComponentData<LocalToWorld>(parent);

                localPosition -= parentLocalToWorld.Position;
            }
        }
        
        ref var localTransform = ref _systemApi.GetComponentRef<LocalTransform>(entity);
        localTransform.Position = localPosition;
        
        ref var ltw = ref _systemApi.GetComponentRef<LocalToWorld>(entity);
        ltw.Value = float4x4.Translate(worldPosition);
    }
}
