using Unity.Entities;
using Unity.Jobs;

// ReSharper disable MemberCanBeMadeStatic.Global

namespace TMUtilsEcs.Ecs
{
    public partial class SystemApiServiceSystem : SystemBase
    {
        public JobHandle J
        {
            get => Dependency;
            set => Dependency = value;
        }

        public new TComponent GetSingleton<TComponent>()
            where TComponent : unmanaged, IComponentData
            => SystemAPI.GetSingleton<TComponent>();

        public new ref TComponent GetSingletonRW<TComponent>()
            where TComponent : unmanaged, IComponentData
            => ref SystemAPI.GetSingletonRW<TComponent>().ValueRW;

        public TComponent? TryGetSingleton<TComponent>()
            where TComponent : unmanaged, IComponentData
        {
            if (!SystemAPI.TryGetSingleton<TComponent>(out var result))
                return null;

            return result;
        }

        public new Entity GetSingletonEntity<TComponent>()
            where TComponent : unmanaged, IComponentData
            => SystemAPI.GetSingletonEntity<TComponent>();

        public Entity TryGetSingletonEntity<TComponent>()
            where TComponent : unmanaged, IComponentData
        {
            if (!SystemAPI.TryGetSingletonEntity<TComponent>(out var result))
                return Entity.Null;

            return result;
        }

        public TAspect GetSingletonAspect<TAspect, TComponent>()
            where TAspect : unmanaged, IAspect, IAspectCreate<TAspect>
            where TComponent : unmanaged, IComponentData
            => EntityManager.GetAspect<TAspect>(GetSingletonEntity<TComponent>());

        public ref TComponent GetComponentRef<TComponent>(Entity entity)
            where TComponent : unmanaged, IComponentData
            => ref SystemApiExtensions.GetComponentRef<TComponent>(this, entity);

        public RefRW<TComponent> GetComponentRefObject<TComponent>(Entity entity)
            where TComponent : unmanaged, IComponentData
            => SystemApiExtensions.GetComponentRefObject<TComponent>(this, entity);

        public new EntityQuery GetEntityQuery(params ComponentType[] componentTypes)
            => base.GetEntityQuery(componentTypes);

        protected override void OnUpdate()
        {
        }
    }
}