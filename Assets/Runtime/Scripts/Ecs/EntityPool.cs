﻿using SmokGnu.EcsUtils.Debug;
using System;
using Unity.Assertions;
using Unity.Collections;
using Unity.Entities;

namespace SmokGnu.EcsUtils.Ecs
{
    public class EntityPool : IDisposable
    {
        private const int DefaultSize = 10;
        private readonly EntityManager _entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        private EntityCommandBufferSystem _ecbSystem;
        private NativeList<Entity> _entities;
        private readonly InstantiateImpl _instantiateImpl;
        private readonly Action<Entity> _onDispose;
        private readonly Action<Entity> _onInitialize;
        public InstantiateImpl InstantiateImpl => _instantiateImpl;

        // STRUCTURAL CHANGE
        public EntityPool(EntityArchetype? archetype = null,
            Entity? prefab = null,
            EntityCommandBufferSystem ecbSystem = null,
            int size = DefaultSize,
            string entityName = null,
            Action<Entity> onInitialize = null,
            Action<Entity> onDispose = null)
        {
            if (archetype.HasValue)
                _instantiateImpl.Archetype = archetype.Value;
            else
                _instantiateImpl.Prefab = prefab.Value;

            _instantiateImpl.EntityName = entityName;
            InitInConstructor(size, ecbSystem);

            _onInitialize = onInitialize;
            _onDispose = onDispose;
        }

        public void Dispose()
            => _entities.Dispose();

        // STRUCTURAL CHANGE
        // O(taken Entities count)
        public Entity TakeNextFreeEntity()
        {
            var firstFreeEntity = Entity.Null;
            foreach (var entity in _entities)
            {
                if (!_entityManager.IsEnabled(entity))
                {
                    firstFreeEntity = entity;
                    break;
                }
            }

            if (firstFreeEntity == Entity.Null)
            {
                // optim: make configurable resize step, and choose sensible default
                var firstNewEntityIndex = _entities.Length;
                _entities.ResizeUninitialized(_entities.Length * 2);
                Instantiate(firstNewEntityIndex);
                firstFreeEntity = _entities[firstNewEntityIndex];
            }

            _entityManager.SetEnabled(firstFreeEntity, true);
            _onInitialize?.Invoke(firstFreeEntity);
            return firstFreeEntity;
        }

        // TODO: TakeNextFreeEntity array overload

        // STRUCTURAL CHANGE
        public void ReturnEntity(Entity entity)
        {
            Assert.IsTrue(_entities.Contains(entity));
            SetEnabled(entity, false);
            _onDispose?.Invoke(entity);
        }

        public void ReturnAll()
        {
            foreach (var entity in _entities)
            {
                if (_entityManager.IsEnabled(entity))
                    ReturnEntity(entity);
            }
        }

        private void Instantiate(int startIndex)
        {
            var newEntitiesSlice =
                _entities.AsArray().GetSubArray(startIndex, _entities.Length - startIndex);
            InstantiateImpl.Instantiate(newEntitiesSlice, _entityManager);

            foreach (var entity in newEntitiesSlice)
            {
                _entityManager.SetEnabled(entity, false);
            }
        }

        private void InitInConstructor(int size, EntityCommandBufferSystem ecbSystem)
        {
            if (size < 1)
            {
                UnityEngine.Debug.LogError("size cannot be < 1");
                return;
            }

            _ecbSystem = ecbSystem;
            _entities = new NativeList<Entity>(size, Allocator.Persistent);
            _entities.ResizeUninitialized(size);
            Instantiate(0);
        }

        private void SetEnabled(Entity entity, bool enabled)
        {
            if (_ecbSystem == null)
            {
                _entityManager.SetEnabled(entity, enabled);
            }
            else
            {
                var ecb = _ecbSystem.CreateCommandBuffer();
                ecb.SetEnabled(entity, enabled);
            }
        }
    }

    public struct InstantiateImpl
    {
        public EntityArchetype Archetype;
        public string EntityName;
        public Entity Prefab;

        public void Instantiate(NativeArray<Entity> outEntities, EntityManager eManager)
        {
            if (Prefab != Entity.Null)
            {
                eManager.Instantiate(Prefab, outEntities);
            }
            else
            {
                eManager.CreateEntity(Archetype, outEntities);
            }

            if (EntityName != null)
            {
                foreach (var entity in outEntities)
                    eManager.SetNameIndexed(entity, EntityName);
            }
        }
    }
}