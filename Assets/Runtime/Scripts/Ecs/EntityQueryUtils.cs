﻿using System;
using Unity.Collections;
using Unity.Entities;

namespace SmokGnu.EcsUtils.Ecs
{
    public delegate void RefAction<T>(ref T item);

    public static class EntityQueryUtils
    {
        public static DynamicBuffer<TReinterpret> GetSingletonBufferReinterpreted<TReinterpret, T>(this SystemBase system) 
            where T : unmanaged, IBufferElementData
            where TReinterpret : unmanaged
        {
            var entity = system.GetSingletonEntity<T>();
            return system.EntityManager.GetBuffer<T>(entity).Reinterpret<TReinterpret>();
        }

        public static (TComponent, bool success) GetFirstMatchingComponent<TComponent>(this EntityQuery query)
            where TComponent : unmanaged, IComponentData
        {
            using var matching = query.ToComponentDataArray<TComponent>(Allocator.TempJob);
            return matching.Length == 0 ? default : (matching[0], true);
        }

        // using DefaultGameObjectInjectionWorld
        public static (TComponent, bool success) GetFirstMatchingComponent<TComponent>()
            where TComponent : unmanaged, IComponentData
        {
            var query = World.DefaultGameObjectInjectionWorld.EntityManager.CreateEntityQuery(typeof(TComponent));
            return query.GetFirstMatchingComponent<TComponent>();
        }

        public static (TComponent, bool success) GetFirstMatchingComponentLogFailure<TComponent>(this EntityQuery query)
            where TComponent : unmanaged, IComponentData
        {
            var findRes = query.GetFirstMatchingComponent<TComponent>();
            if (!findRes.success)
            {
                UnityEngine.Debug.LogError($"{typeof(TComponent).Name} component missing");
            }
            return findRes;
        }

        // using DefaultGameObjectInjectionWorld
        public static (TComponent, bool success) GetFirstMatchingComponentLogFailure<TComponent>()
            where TComponent : unmanaged, IComponentData
        {
            var query = World.DefaultGameObjectInjectionWorld.EntityManager.CreateEntityQuery(typeof(TComponent));
            return query.GetFirstMatchingComponentLogFailure<TComponent>();
        }

        /// <returns>Null if not found</returns>
        public static Entity GetFirstEntityWithComponent(this EntityQuery query)
        {
            using var matching = query.ToEntityArray(Allocator.TempJob);
            return matching.Length == 0 ? Entity.Null : matching[0];
        }

        public static Entity GetFirstEntityWithComponent<TComponent>()
        {
            var query = World.DefaultGameObjectInjectionWorld.EntityManager.CreateEntityQuery(typeof(TComponent));
            return query.GetFirstEntityWithComponent();
        }

        public static Entity GetFirstEntityWithComponentLogFailure<TComponent>(this EntityQuery query)
        {
            var entity = query.GetFirstEntityWithComponent();
            if (entity == Entity.Null)
            {
                UnityEngine.Debug.LogError($"{typeof(TComponent).Name} component missing");
            }
            return entity;
        }

        public static Entity GetFirstEntityWithComponentLogFailure<TComponent>()
        {
            var query = World.DefaultGameObjectInjectionWorld.EntityManager.CreateEntityQuery(typeof(TComponent));
            return query.GetFirstEntityWithComponentLogFailure<TComponent>();
        }

        // this will produce garbage if predicate captures
        public static Entity GetFirstEntityWithComponent<TFilterComponent>(this EntityQuery query, Func<Entity, TFilterComponent, bool> predicate)
            where TFilterComponent : unmanaged, IComponentData
        {
            using var matchingComponents = query.ToComponentDataArray<TFilterComponent>(Allocator.Temp);
            using var matchingEntities = query.ToEntityArray(Allocator.Temp);
            UnityEngine.Debug.Assert(matchingComponents.Length == matchingEntities.Length);

            for (var i = 0; i < matchingEntities.Length; i++)
            {
                if (predicate(matchingEntities[i], matchingComponents[i]))
                {
                    return matchingEntities[i];
                }
            }
            return Entity.Null;
        }

        public static NativeArray<Entity> GetEntitiesWithComponent<TComponent>()
        {
            var query = World.DefaultGameObjectInjectionWorld.EntityManager.CreateEntityQuery(typeof(TComponent));
            return query.ToEntityArray(Allocator.TempJob);
        }

    }
}