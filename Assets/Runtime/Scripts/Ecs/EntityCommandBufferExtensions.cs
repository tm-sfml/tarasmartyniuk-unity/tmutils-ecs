﻿using Unity.Entities;

namespace SmokGnu.EcsUtils.Ecs
{
    public static class EntityCommandBufferExtensions
    {
        public static void SetEnabled(this EntityCommandBuffer ecb, Entity entity, bool enabled)
        {
            var eManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            if (eManager.HasComponent<Disabled>(entity) == !enabled)
                return;

            if (!enabled)
                ecb.AddComponent<Disabled>(entity);
            else
                ecb.RemoveComponent<Disabled>(entity);
        }
    }
}
