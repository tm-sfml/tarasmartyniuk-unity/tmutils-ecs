﻿using SmokGnu.EcsUtils.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace SmokGnu.EcsUtils.Ecs
{
    public static class EntityTransformUtils
    {
        // public static void SetGlobalPosition(Entity entity, float3 worldPosition)
        // {
        //     var eManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        //
        //     if (eManager.HasComponent<LocalToParent>(entity))
        //     {
        //         var parent = eManager.GetComponentData<Parent>(entity).Value;
        //         if (parent != Entity.Null)
        //         {
        //             var parentPosition = eManager.GetComponentData<LocalToWorld>(parent).Position;
        //             worldPosition -= parentPosition;
        //         }
        //     }
        //     eManager.SetComponentData(entity, new Translation { Value = worldPosition });
        // }

        // public static void SetRootPositionUpdateLTW(Entity entity, float3 worldPosition)
        // {
        //     var eManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        //     eManager.SetComponentData(entity, new Translation { Value = worldPosition });
        //     
        //     using var ltwRef = new ComponentReference<LocalToWorld>(entity);
        //     ref var ltw = ref ltwRef.Value();
        //     ltw.Value = float4x4.TRS(worldPosition, ltw.Rotation, ltw.Value.GetScale());
        // }

        // public static void SetChildPositionUpdateLTW(Entity entity, float3 pos, quaternion rot, float3 scale)
        // {
        //     var eManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        //     SetLTWComponents(entity, pos, rot, scale);
        //     var localToParent = float4x4.TRS(pos, rot, scale);
        //     eManager.SetComponentData(entity, new LocalToParent { Value = localToParent});
        //
        //     var parent = eManager.GetComponentData<Parent>(entity).Value;
        //     var parentLtw = eManager.GetComponentData<LocalToWorld>(parent).Value;
        //     var ltw = math.mul(parentLtw, localToParent);
        //     eManager.SetComponentData(entity, new LocalToWorld { Value = ltw });  
        // }


        // public static bool IsRoot(Entity entity) => !IsChild(entity);

        // public static bool IsChild(Entity entity)
        // {
        //     var eManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        //     return eManager.HasComponent<LocalToParent>(entity) && eManager.HasComponent<Parent>(entity);
        // }

        // public static void ChangeToRoot(Entity entity)
        // {
        //     var eManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        //     eManager.RemoveComponent<LocalToParent>(entity);
        //     eManager.RemoveComponent<Parent>(entity);
        // }
        //
        // public static void ChangeToChild(Entity entity, Entity? parent = null)
        // {
        //     var eManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        //     eManager.AddComponentData(entity, new Parent { Value = parent.GetValueOrDefault(Entity.Null) });
        //     eManager.AddComponentData(entity, new LocalToParent());
        // }

        public static DynamicBuffer<Entity> GetChildren(Entity parent) => DotsCollectionUtils.GetReinterprettedBuffer<Entity, Child>(parent);

        public static void SetParent(Entity child, Entity parent, bool updateChildBuffer = false)
        {
            var eManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            eManager.SetComponentData(child, new Parent { Value = parent });

            if (!updateChildBuffer)
                return;

            var children = eManager.AddBuffer<Child>(parent).Reinterpret<Entity>();

            UnityEngine.Debug.Assert(!children.Contains(child));
            children.Add(child);
        }

        public static void SetupChildBuffers(Entity root)
        {
            var eManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            var linkedGroup = eManager.GetBuffer<LinkedEntityGroup>(root).Reinterpret<Entity>();

            foreach (var entity in linkedGroup)
            {
                if (!eManager.HasComponent<Parent>(entity))
                    continue;

                var parent = eManager.GetComponentData<Parent>(entity).Value;
                var parentsChildren = eManager.AddBuffer<Child>(parent).Reinterpret<Entity>();
                parentsChildren.Add(entity);
            }
        }

        public static Entity GetParent(Entity child)
        {
            var eManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            return eManager.GetComponentData<Parent>(child).Value;
        }

        public static void DetachChildren(Entity parent)
        {
            var children = GetChildren(parent);
            var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            foreach (var child in children)
            {
                entityManager.SetComponentData(child, new Parent { Value = Entity.Null });
            }
        }

        public static Entity FindChildWithComponent<T>(this EntityManager entityManager, Entity entity)
        {
            if (!entityManager.HasComponent<Child>(entity))
            {
                return Entity.Null;
            }

            var children = entityManager.GetBuffer<Child>(entity);
            foreach (var child in children)
            {
                if (entityManager.HasComponent<T>(child.Value))
                {
                    return child.Value;
                }
            }
            return Entity.Null;
        }

        public static TComponent GetComponentDataInChildren<TComponent>(this EntityManager entityManager, Entity entity)
            where TComponent : unmanaged, IComponentData
        {
            var child = entityManager.FindChildWithComponent<TComponent>(entity);
            return entityManager.GetComponentData<TComponent>(child);
        }

        // searches through LinkedEntityGroup buffer
        public static Entity FindLinkedWithComponent<T>(this EntityManager entityManager, Entity entity)
        {
            if (!entityManager.HasComponent<LinkedEntityGroup>(entity))
            {
                return Entity.Null;
            }

            var linkedGroupBuffer = entityManager.GetBuffer<LinkedEntityGroup>(entity);
            foreach (var linked in linkedGroupBuffer)
            {
                if (entityManager.HasComponent<T>(linked.Value))
                {
                    return linked.Value;
                }
            }
            return Entity.Null;
        }
    }
}
