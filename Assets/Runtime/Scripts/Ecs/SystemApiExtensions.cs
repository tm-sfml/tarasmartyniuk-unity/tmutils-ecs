using Unity.Entities;

namespace TMUtilsEcs.Ecs
{
    public static class SystemApiExtensions
    {
        public static ref TComponent GetComponentRef<TComponent>(this SystemBase system, Entity entity)
            where TComponent : unmanaged, IComponentData
            => ref GetComponentRefObject<TComponent>(system, entity).ValueRW;

        public static RefRW<TComponent> GetComponentRefObject<TComponent>(this SystemBase system, Entity entity)
            where TComponent : unmanaged, IComponentData
        {
            var lookup = system.GetComponentLookup<TComponent>();
            return lookup.GetRefRW(entity);
        }
    }
}