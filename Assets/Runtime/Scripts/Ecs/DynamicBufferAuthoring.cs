﻿

// public class DynamicBufferAuthoring<T> : MonoBehaviour, IConvertGameObjectToEntity
//     where T : unmanaged, IBufferElementData
// {
//     [SerializeField] List<T> Buffer;
//
//     public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem _)
//     {
//         var buffer = dstManager.AddBuffer<T>(entity);
//         // it is null when T is not serializable
//         if (Buffer != null)
//         {
//             buffer.ResizeUninitialized(Buffer.Count);
//             foreach (var inputElement in Buffer)
//             {
//                 buffer.Add(inputElement);
//             }
//         }
//     }
// }
