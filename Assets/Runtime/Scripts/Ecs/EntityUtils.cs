﻿using Unity.Entities;

namespace SmokGnu.EcsUtils.Ecs
{
    public static class EntityUtils
    {
        public static void CopyComponent<T>(this EntityManager eManager, Entity from, Entity to)
            where T : unmanaged, IComponentData
        {
            eManager.SetComponentData(to, eManager.GetComponentData<T>(from));
        }
    }
}
