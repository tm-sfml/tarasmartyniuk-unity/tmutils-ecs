﻿using System.Collections.Generic;
using TMUtils.Debug;
using TMUtils.Singletons;
using Unity.Entities;
using Unity.Transforms;

namespace SmokGnu.EcsUtils.Debug
{
    public class EntityVisualization : MonobehaviorSingletonBase<EntityVisualization>
    {
        public List<Entity> EntitiesToDraw = new();

        private void Update()
        {
            var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            EntitiesToDraw.RemoveAll(e => !World.DefaultGameObjectInjectionWorld.EntityManager.Exists(e));
            for (var i = 0; i < EntitiesToDraw.Count; i++)
            {
                DebugX.DrawPoint(entityManager.GetComponentData<LocalToWorld>(EntitiesToDraw[i]).Position, DebugColors.Instance.GetColor(i), .1f, 2f / 60f);
            }
        }
    }
}