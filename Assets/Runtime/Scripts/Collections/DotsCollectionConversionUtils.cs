using System;
using System.Collections.Generic;
using System.Linq;
using Unity.Collections;
using Unity.Entities;

namespace TMUtilsEcs.Collections
{
    public static class DotsCollectionConversionUtils
    {
        public static NativeList<TSource> ToNativeList<TSource>(this IEnumerable<TSource> source, Allocator allocator)
            where TSource : unmanaged
            => source.ToNativeList(source.Count(), allocator);

        public static NativeList<TSource> ToNativeList<TSource>(this IEnumerable<TSource> source, int count,
            Allocator allocator)
            where TSource : unmanaged
        {
            var list = new NativeList<TSource>(count, allocator);
            foreach (var element in source)
                list.Add(element);

            return list;
        }

        public static NativeList<TSource> ToNativeList<TSource>(this NativeHashSet<TSource> source, int count,
            Allocator allocator)
            where TSource : unmanaged, IEquatable<TSource>
        {
            var list = new NativeList<TSource>(count, allocator);
            list.CopyFrom(source);
            return list;
        }

        public static void CopyFrom<TSource>(this NativeList<TSource> list, NativeHashSet<TSource> source)
            where TSource : unmanaged, IEquatable<TSource>
        {
            list.Clear();

            foreach (var element in source)
                list.Add(element);
        }

        public static T[] ToManagedArray<T>(this NativeList<T> list)
            where T : unmanaged
            => list.AsArray().ToArray();

        public static T[] ToManagedArray<T>(this DynamicBuffer<T> list)
            where T : unmanaged
            => list.AsNativeArray().ToArray();
    }
}