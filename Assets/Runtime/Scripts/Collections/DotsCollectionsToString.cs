﻿using System.Text;
using TMUtils.Utils.Math;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

namespace SmokGnu.EcsUtils.Collections
{
    public static class DotsCollectionsToString
    {
        public static string Stringify(this Entity e)
        {
            return World.DefaultGameObjectInjectionWorld.EntityManager.GetName(e);
        }
    
        public static string Stringify<T>(this NativeArray<T> array)
            where T : unmanaged
        {
            var sb = new StringBuilder('[');
            foreach (var item in array)
            {   
                sb.Append(item);
                sb.Append(", ");
            }
            sb.Append(']');
            return sb.ToString();
        }

        public static string Stringify<T>(this DynamicBuffer<T> buff)
            where T : unmanaged
            => Stringify(buff.AsNativeArray());

        public static string Stringify<T>(this NativeList<T> list)
            where T : unmanaged
            => Stringify(list.AsArray());

        public static string Stringify<T>(this NativeSlice2D<T> slice2D, BoundsInt2D? bounds = null)
            where T : unmanaged
        {
            var sb = new StringBuilder('[');
            bounds ??= new BoundsInt2D(int2.zero, slice2D.Size);
            var (botLeft, topRight) = bounds.Value;
            for (var y = topRight.y; y >= botLeft.y; y--)
            {
                for (var x = botLeft.x; x < topRight.x; x++)
                {
                    sb.Append(slice2D[x, y]);
                }

                sb.AppendLine();
            }
            sb.Append(']');
            return sb.ToString();
        }

    }
}
