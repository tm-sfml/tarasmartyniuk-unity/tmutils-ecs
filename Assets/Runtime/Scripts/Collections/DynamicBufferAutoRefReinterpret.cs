﻿using Unity.Entities;

namespace SmokGnu.EcsUtils.Collections
{
    public ref struct DynamicBufferAutoRefReinterpret<TReinterpret, T> where T : unmanaged, IBufferElementData
        where TReinterpret : unmanaged
    {
        public Entity Entity { get; private set; }
        public DynamicBuffer<TReinterpret> Value => DotsCollectionUtils.GetReinterprettedBuffer<TReinterpret, T>(Entity);

        public DynamicBufferAutoRefReinterpret(Entity entity)
        {
            Entity = entity;
        }
    }
}
