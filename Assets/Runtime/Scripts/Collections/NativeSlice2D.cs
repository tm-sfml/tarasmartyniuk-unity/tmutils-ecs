﻿using TMUtils.Utils.Collections;
using Unity.Collections;
using Unity.Mathematics;

namespace SmokGnu.EcsUtils.Collections
{
    /// <summary>
    ///     adapter that interprets array(buffer) as 2D, without owning it
    /// </summary>
    public struct NativeSlice2D<TElem> where TElem : unmanaged
    {
        private NativeArray<TElem> m_flatSlice;
        public NativeSlice<TElem> FlatSlice => m_flatSlice;

        public int2 Size { get; }

        public NativeSlice2D(int2 size, NativeArray<TElem> flatSlice)
        {
            m_flatSlice = flatSlice;
            Size = size;
        }

        public NativeSlice2D(int sizeX, NativeArray<TElem> flatSlice)
            : this(new int2(sizeX, flatSlice.Length / sizeX), flatSlice)
        {
        }

        public TElem this[int2 index]
        {
            get => m_flatSlice[FlattenIndex(index.x, index.y)];
            set => m_flatSlice[FlattenIndex(index.x, index.y)] = value;
        }

        public TElem this[int x, int y]
        {
            get => m_flatSlice[FlattenIndex(x, y)];
            set => m_flatSlice[FlattenIndex(x, y)] = value;
        }

        public ref TElem ElementAt(int x, int y)
            => ref m_flatSlice.ElementAt(FlattenIndex(x, y));

        public ref TElem ElementAt(int2 xy)
            => ref ElementAt(xy.x, xy.y);

        public NativeSlice<TElem> this[int x]
        {
            get
            {
                var yStart = FlattenIndex(x, 0);
                return new NativeSlice<TElem>(m_flatSlice, yStart, Size.y);
            }
        }

        public bool IsValidIndex(int x, int y)
            => x >= 0 &&
               y >= 0 &&
               x < Size.x &&
               y < Size.y;

        public bool IsValidIndex(int2 index)
            => IsValidIndex(index.x, index.y);

        public void Fill(in TElem value)
            => FlatSlice.Fill(value);

        public int FlattenIndex(int x, int y)
            => List2DUtils.Flatten2DIndex(x, y, Size.y);

        public int FlattenIndex(int2 i)
            => List2DUtils.Flatten2DIndex(i.x, i.y, Size.y);
    }

    public static class SliceExtensions
    {
        public static void Fill<T>(this NativeSlice<T> slice, in T value) where T : unmanaged
        {
            for (var i = 0; i < slice.Length; i++)
            {
                slice[i] = value;
            }
        }
    }
}