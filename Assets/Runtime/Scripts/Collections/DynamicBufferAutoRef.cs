﻿using Unity.Entities;

namespace SmokGnu.EcsUtils.Collections
{
    public struct DynamicBufferAutoRef<T> where T : unmanaged, IBufferElementData
    {
        public Entity Entity { get; private set; }
        public DynamicBuffer<T> Value => World.DefaultGameObjectInjectionWorld.EntityManager.GetBuffer<T>(Entity);

        public DynamicBufferAutoRef(Entity entity)
        {
            Entity = entity;
        }
    }
}
