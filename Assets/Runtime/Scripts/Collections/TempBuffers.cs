﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMUtils.Singletons;

namespace SmokGnu.EcsUtils.Collections
{
    public class TempBuffers : SingletonBase<TempBuffers>
    {
        const int m_defaultCapacity = 20;

        Dictionary<Type, IList> m_typeToList = new();

        public List<T> GetList<T>()
        {
            var result = GetCollection(m_typeToList, () => new List<T>(m_defaultCapacity));
            result.Clear();
            return result;
        }

        private static TConcrete GetCollection<TConcrete, TBase>(Dictionary<Type, TBase> typeToCollection, Func<TConcrete> createCollection)
            where TConcrete : TBase
        {
            var type = typeof(TConcrete);
            if (typeToCollection.TryGetValue(type, out var baseList))
                return (TConcrete) baseList;

            var result = createCollection();
            typeToCollection[type] = result;
            return result;
        }
    }
}
