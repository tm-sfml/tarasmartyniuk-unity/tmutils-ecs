﻿using Unity.Collections;
using Unity.Mathematics;

// adapter that interprets array(buffer) as 3D, without owning it
namespace SmokGnu.EcsUtils.Collections
{
    public struct NativeSlice3D<TElem> where TElem : unmanaged
    {
        public NativeSlice<TElem> FlatSlice => m_flatSlice;
        public int3 Size { get; }

        NativeArray<TElem> m_flatSlice;

        public NativeSlice3D(int3 size, NativeArray<TElem> flatSlice)
        {
            UnityEngine.Debug.Assert(size.x * size.y * size.z == flatSlice.Length);

            m_flatSlice = flatSlice;
            Size = size;
        }

        public NativeSlice2D<TElem> this[int x]
        {
            get
            {
                var yzLength = Size.y * Size.z;
                var yzStart = x * yzLength;

                var yzFlatSlice = m_flatSlice.GetSubArray(yzStart, yzLength);
                return new NativeSlice2D<TElem>(Size.y, yzFlatSlice);
            }
        }

        public NativeSlice<TElem> this[int x, int y]
        {
            get
            {
                var yzLength = Size.y * Size.z;
                var yzStart = x * yzLength;
                var offset = y * Size.z;

                var zStart = yzStart + offset;
                var zSlice = new NativeSlice<TElem>(m_flatSlice, zStart, Size.z);
                return zSlice;
            }
        }

        public TElem this[int x, int y, int z]
        {
            get
            {
                var zSlice = this[x, y];
                return zSlice[z];
            }

            set
            {
                var zSlice = this[x, y];
                zSlice[z] = value;
            }
        }

        public TElem this[int3 index]
        {
            get => this[index.x, index.y, index.z];
            set => this[index.x, index.y, index.z] = value;
        }

        public NativeSlice<TElem> this[int2 xy] => this[xy.x, xy.y];
        public int3 UnflattenIndex(int i)
        {
            var yzLength = Size.y * Size.z;
            var x = i / yzLength;
            var yzStart = x * yzLength;
            var yzLengthToI = i - yzStart;
            var y = yzLengthToI / Size.z;
            var z = i % Size.z;

            return new int3(x, y, z);
        }

        // TODO: public bool IsValidIndex(int index)

        public void Fill(in TElem value)
        {
            FlatSlice.Fill(value);
        }
    }
}

