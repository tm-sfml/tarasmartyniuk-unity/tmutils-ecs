﻿using Unity.Entities;

namespace SmokGnu.EcsUtils.Collections
{
    public static class DynamicBufferQueueAdapterExtensions
    {
        public static ref T Enqueue<T>(this DynamicBuffer<T> queue, T element)
            where T : unmanaged
        {
            queue.Add(element);
            return ref PeekLast(queue);
        }

        public static T Dequeue<T>(this DynamicBuffer<T> queue)
            where T : unmanaged
        {
            var result = queue[0];
            queue.RemoveAt(0);
            return result;
        }

        public static ref T PeekFirst<T>(this DynamicBuffer<T> queue)
            where T : unmanaged =>
            ref queue.ElementAt(0);

        public static ref T PeekLast<T>(this DynamicBuffer<T> queue)
            where T : unmanaged =>
            ref queue.ElementAt(queue.Length - 1);
    }
}
