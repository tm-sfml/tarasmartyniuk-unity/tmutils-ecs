﻿using NUnit.Framework;
using System;
using System.Collections;
using UnityEngine;

namespace Tests.Shared
{
    public static class WaitForUtils
    {
        public static IEnumerator WaitUntilWithTimeout(Func<bool> condition, float timeout)
        {
            float startTime = Time.realtimeSinceStartup;
            bool TimeoutPredicate()
            {
                float timePassed = Time.realtimeSinceStartup - startTime;
                if (timePassed >= timeout)
                {
                    Assert.Fail($"WaitUntilWithTimeout timeout after {timeout}s");
                    return true;
                }

                bool checkResult = condition();
                if (checkResult)
                {
                    TestContext.Out.WriteLine($"WaitUntilWithTimeout: time passed: {timePassed}");
                }
                return checkResult;
            }

            yield return new WaitUntil(TimeoutPredicate);
        }
    }
}