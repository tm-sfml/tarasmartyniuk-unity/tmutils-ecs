﻿using NUnit.Framework;
using Unity.Mathematics;

namespace Tests.Edit
{
    public class math
    {
        [Test]
        public void Rotate_Int2_90()
        {
            Assert.AreEqual(new int2(1, -4), new int2(4, 1).RotateDegrees(90));
        }

        [Test]
        public void Rotate_Int2_Rounds()
        {
            Assert.AreEqual(new int2(18, -3), new int2(3, 18).RotateDegrees(90));
        }

        [Test]
        public void Rotate_Float2_90()
        {
            Assert.IsTrue(new float2(4.5f, 3.3f).RotateDegrees(90).Approximately(new float2(3.3f, -4.5f)));
        }
        
    }
}