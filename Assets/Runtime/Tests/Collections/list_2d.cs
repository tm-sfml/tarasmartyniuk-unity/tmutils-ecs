﻿using NUnit.Framework;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;

namespace Tests.Edit
{
    public class list_2d
    {
        [Test]
        public void UnflattenIndex_IsInverseOfFlattenIndex()
        {
            var list = new List2D<int>(4, 5);
            for (int i = 0; i < list.Size1D; i++)
            {
                for (int j = 0; j < list.Size2D; j++)
                {
                    int flattened = list.FlattenIndex(i, j);
                    Assert.AreEqual(new int2(i, j), list.UnflattenIndex(flattened));
                }
            }
        }
    }
}